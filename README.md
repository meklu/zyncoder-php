# zyncoder

Allows you to zyncode things with a custom base64 -like system.

## Usage

    // encode a message
    Zyncoder::enc("zyn:D");
    // decode a message
    Zyncoder::dec("ZыN зYн ЗЫN зЫН zЫн зYn Zyn)");

## WTF?

There's an interactive demo now available at https://zyn.pilipali.io/zyncoder/
