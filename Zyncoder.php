<?php

if (!class_exists("Zyncoder")) {
	class Zyncoder {
		private static function Z() {
			return ['z', 'Z', 'з', 'З'];
		}

		private static function Y() {
			return ['y', 'Y', 'ы', 'Ы'];
		}

		private static function N() {
			return ['n', 'N', 'н', 'Н'];
		}

		private static function chars() {
			static $t = null;
			if ($t !== null) { return $t; }
			$Z = self::Z();
			$Y = self::Y();
			$N = self::N();

			return ($t = array_merge($Z, $Y, $N));
		}

		public static function table() {
			static $t = null;
			if ($t !== null) { return $t; }

			$Z = self::Z();
			$Y = self::Y();
			$N = self::N();

			$t = [];
			$c = 0;
			for ($z = 0; $z < count($Z); $z += 1) {
				for ($y = 0; $y < count($Y); $y += 1) {
					for ($n = 0; $n < count($N); $n += 1) {
						$t[$c] = "{$Z[$z]}{$Y[$y]}{$N[$n]}";
						$c += 1;
					}
				}
			}

			return $t;
		}

		public static function rtable() {
			static $t = null;
			if ($t !== null) { return $t; }

			return ($t = array_flip(self::table()));
		}

		private static function trip2zyn($triplet) {
			$len = count($triplet);
			$triplet = array_merge($triplet, [0, 0, 0]);
			$lkup = self::table();
			$out = [];
			$out[] = $lkup[($triplet[0] & 0xFC) >> 2];
			$out[] = $lkup[(($triplet[0] & 0x03) << 4) | (($triplet[1] & 0xF0) >> 4)];
			if ($len >= 2) {
				$out[] = $lkup[(($triplet[1] & 0x0F) << 2) | (($triplet[2] & 0xC0) >> 6)];
				if ($len >= 3) {
					$out[] = $lkup[$triplet[2] & 0x3F];
				} else {
					$out[count($out) - 1] .= ")";
				}
			} else {
				$out[count($out) - 1] .= "))";
			}
			return $out;
		}

		private static function zyn2trip($zyn) {
			foreach ($zyn as $k => $v) {
				if ($v === ")") {
					unset($zyn[$k]);
				}
			}
			unset($k, $v);

			$len = count($zyn);
			$lkup = self::rtable();
			$zn = [];
			foreach ($zyn as $z) {
				$zn[] = $lkup[$z];
			}
			$zn = array_merge($zn, [0, 0, 0]);
			$out = [];
			$out[] = chr( (($zn[0] & 0x3F) << 2) | (($zn[1] & 0x30) >> 4) );
			if ($len >= 3) {
				$out[] = chr( (($zn[1] & 0x0F) << 4) | (($zn[2] & 0x3C) >> 2) );
				if ($len >= 4) {
					$out[] = chr( (($zn[2] & 0x03) << 6) | ($zn[3] & 0x3F) );
				}
			}
			return $out;
		}

		public static function enc($bs) {
			$lkup = self::table();
			$acc = [];
			$out = [];
			for ($pos = 0; $pos < strlen($bs); $pos += 1) {
				$b = ord(substr($bs, $pos, 1));
				$acc[] = $b;
				if (count($acc) === 3) {
					foreach (self::trip2zyn($acc) as $o) {
						$out[] = $o;
					}
					$acc = [];
				}
			}
			if (count($acc) > 0) {
				foreach (self::trip2zyn($acc) as $o) {
					$out[] = $o;
				}
			}

			return implode(" ", $out);
		}

		public static function dec($bs, $harsh = false) {
			$acc = [];
			$zyns = [];
			$out = [];

			$ws = " \t\n\r";
			$ck = [self::Z(), self::Y(), self::N()];
			$cnt = 0;

			for ($pos = 0; $pos < mb_strlen($bs); $pos += 1) {
				$c = mb_substr($bs, $pos, 1);
				if (!in_array($c, $ck[$cnt % 3])) {
					if ($harsh === true && mb_strpos($ws, $c) === false) {
						if ($c !== ")" || mb_strlen($bs) - $pos > 2) {
							return false;
						}
					}
					continue;
				}
				$cnt += 1;
				$acc[] = $c;
				if (count($acc) === 3) {
					$zyns[] = implode($acc);
					$acc = [];
				}
				if (count($zyns) === 4) {
					foreach (self::zyn2trip($zyns) as $o) {
						$out[] = $o;
					}
					$zyns = [];
				}
			}
			if ($harsh === true && count($acc) > 0) {
				return false;
			}
			if (count($zyns) > 0) {
				foreach (self::zyn2trip($zyns) as $o) {
					$out[] = $o;
				}
			}
			if ($harsh === true) {
				$n = 4 - count($zyns);
				if ($n > 2) {
					return false;
				}
				if (0 !== strncmp(mb_substr($bs, -$n), "))", $n)) {
					return false;
				}
			}
			return implode($out);
		}
	}
}
